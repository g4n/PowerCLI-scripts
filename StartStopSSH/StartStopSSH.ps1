#cut-and-paste-powercli
#g4n 2018-01-19

# StartStopSSH.ps1
# PowerCLI commands to start and stop SSH service on ESXi hosts

# SSH service on a single ESXi host

# query SSH status on a host
get-vmhostservice -vmhost 'esxhost' | where {$_.key -eq 'TSM-SSH'}
# start SSH on a host
get-vmhostservice -vmhost 'esxhost' | where {$_.key -eq 'TSM-SSH'} | start-vmhostservice
# stop SSH on a host
get-vmhostservice -vmhost 'esxhost' | where {$_.key -eq 'TSM-SSH'} | stop-vmhostservice -confirm:$false

# SSH service on every ESXi host of a cluster

# query SSH status on a cluster
get-vmhost -location 'cluster' | foreach { get-vmhostservice -vmhost $_ | where {$_.key -eq 'TSM-SSH'} }
# start SSH on a cluster
get-vmhost -location 'cluster' | foreach { get-vmhostservice -vmhost $_ | where {$_.key -eq 'TSM-SSH'} | start-vmhostservice }
# stop SSH on a cluster
get-vmhost -location 'cluster' | foreach { get-vmhostservice -vmhost $_ | where {$_.key -eq 'TSM-SSH'} | stop-vmhostservice -confirm:$false}

