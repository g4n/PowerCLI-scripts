# [StartStopSSH.ps1](StartStopSSH.ps1)
PowerCLI commands to start and stop SSH service on ESXi hosts.

Just start with the classic
```
connect-viserver myvcenter.localdomain
```
and cut-modify-paste 'cum grano salis' (be careful)