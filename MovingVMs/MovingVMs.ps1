#cut-and-paste-powercli
#g4n 2018-01-19

# MovingVMs.ps1
# PowerCLI commands to move VMs around


### vMotion
### moving the compute part of the VM

# vMotion of a single vm to a new host
move-vm -vm 'vmname' -destination 'esxhost.localdomain'

# piping a VM to Move-VM
get-vm -name 'vmname' | move-vm -destination 'esxhost.localdomain'

# using vars
$vm = get-vm -name 'vmname'
$esx = get-vmhost -name 'esxhost.localdomain'
move-vm -vm $vm -destination $esx

# moving to another host all the VMs with a name starting with T-
get-vm -name 'T-*' | foreach { move-vm -vm $_ -destination 'esxhost.localdomain' }

# moving all the VMs from a host to another host
get-vm -location 'esxhost1.localdomain' | foreach { move-vm -vm $_ -destination 'esxhost2.localdomain' }

# moving all the VMs from a cluster to another cluster (DRS required)
get-vm -location 'cluster1' | foreach { move-vm -vm $_ -destination 'cluster2' }

# moving to another host all the VMs with listed in a text file
get-content 'Desktop\vmlist.txt' | foreach { move-vm -vm $_ -destination 'esxhost.localdomain' }


### Storage vMotion
### moving the storage part of the VM

# svMotion of a single vm to datastore vmfs2
move-vm -vm 'vmname' -datastore 'vmfs2'

# piping a VM to Move-VM
get-vm 'vmname' | Move-VM -Datastore 'vmfs2'

# moving all the VMs from datastore vmfs1 to vmfs2
get-vm -datastore 'vmfs1' | foreach { move-vm -vm $_ -datastore 'vmfs2' }

# moving from datastore vmfs1 to vmfs2 only the VMs with a name starting with T-
get-vm -name 'T-*' -datastore 'vmfs1' | foreach { move-vm -vm $_ -datastore 'vmfs2' }


### Storage vMotion Without Shared Storage
### moving the entire VM (compute+storage) without a common datastore

# svMotion of a single VM to a different host and a different datastore
move-vm -vm 'vmname' -destination 'esxhost.localdomain' -datastore 'vmfs2'

# moving from datastore vmfs1 to vmfs2 while moving to another host (vmfs2 must be accessible from esxhost2)
get-vm -datastore 'vmfs1' | foreach { move-vm -vm $_ -destination 'esxhost2.localdomain' -datastore 'vmfs2' }
