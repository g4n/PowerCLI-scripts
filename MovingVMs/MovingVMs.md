# [MovingVMs.ps1](MovingVMs.ps1)
Various PowerCLI commands to move VMs around, including:
- vMotion for compute only migration
- sVmotion for storage only migration
- Storage Vmotion Without Shared Storage for compute and storage migration

Just start with the classic
```
connect-viserver myvcenter.localdomain
```
and cut-modify-paste 'cum grano salis' (be careful)