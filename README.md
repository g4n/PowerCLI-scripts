# PowerCLI-scripts

Random PowerCLI scripts that may be useful:
- [MovingVMs.ps1](MovingVMs/MovingVMs.md) : to move around VMs
- [StartStopSSH.ps1](StartStopSSH/StartStopSSH.md) : to manage SSH service on hosts